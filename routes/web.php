<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

 Route::get('/register',function(){
   return redirect('/');
 });

 Route::get('/home', 'HomeController@index')->name('home');


 Route::resource('disciplina', 'DisciplinaController');

 Route::resource('aluno', 'AlunoController');

 Route::resource('aluno_nota_disciplina', 'AlunosNotaDisciplinasController');


