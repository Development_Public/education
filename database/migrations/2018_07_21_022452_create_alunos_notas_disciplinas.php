<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosNotasDisciplinas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('alunos_notas_disciplinas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_aluno_disciplina');
            $table->foreign('id_aluno_disciplina')->references('id')->on('alunos_disciplinas');
            $table->double('nota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos_notas_disciplinas');
    }
}
