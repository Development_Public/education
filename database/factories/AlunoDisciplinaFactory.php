<?php

use Faker\Generator as Faker;

$factory->define(App\Models\AlunoDisciplina::class, function (Faker $faker) {
    return [
        'id_aluno'      => 1,
        'id_disciplina' => 3
    ];
});

