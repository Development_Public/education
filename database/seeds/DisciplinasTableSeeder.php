<?php

use Illuminate\Database\Seeder;

class DisciplinasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            ['disciplina' =>'Matemática','sigla' =>'MAT'],
            ['disciplina' =>'Português' ,'sigla' =>'POR'],
            ['disciplina' =>'Física'    ,'sigla' =>'FIS']

        ];

        DB::table('disciplinas')->insert($data);
    }
}
