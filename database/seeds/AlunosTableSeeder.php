<?php

use Illuminate\Database\Seeder;

class AlunosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'matricula'         =>'11111',
                'name'              =>'Renan Gabriel da Mata',
                'endereco'          =>'Quadra QSE 14',
                'bairro'            =>'Taguatinga Sul',
                'cep'               =>'72025-140',
                'cidade'            =>'Taguatinga',
                'unidade_federacao' =>'Distrito Federal',
                'email'             =>'renangabrieldamata_@inbox.com',
                'entrada_escola'    =>'10/08/2018',
            ],
            [
                'matricula'         =>'22222',
                'name'              =>'Alícia Flávia Daniela Lopes',
                'endereco'          =>'Quadra QN 9 Área Especial 11',
                'bairro'            =>'Riacho Fundo I',
                'cep'               =>'71805-821',
                'cidade'            =>'Brasília',
                'unidade_federacao' =>'Distrito Federal',
                'email'             =>'aliciaflaviadanielalopes@inbox.com',
                'entrada_escola'    =>'10/08/2018',
            ],
            [
                'matricula'         =>'33333',
                'name'              =>'Oliver Igor Caio Monteiro',
                'endereco'          =>'Quadra QR 105',
                'bairro'            =>'Samambaia Sul',
                'cep'               =>'72301-100',
                'cidade'            =>'Brasília',
                'unidade_federacao' =>'Distrito Federal',
                'email'             =>'oliverigorcaiomonteiro@3coreonline.com.br',
                'entrada_escola'    =>'10/08/2018',
            ],
            [
                'matricula'         =>'44444',
                'name'              =>'Elaine Gabrielly Stella Brito',
                'endereco'          =>'Rua Maria José',
                'bairro'            =>'Vila São José',
                'cep'               =>'75115-540',
                'cidade'            =>'Anápolis',
                'unidade_federacao' =>'Goiás',
                'email'             =>'elainegabriellystellabrito@yahoo.ca',
                'entrada_escola'    =>'10/08/2018',
            ],
            [
                'matricula'         =>'55555',
                'name'              =>'fernando satos Oliveira',
                'endereco'          =>'Rua 3 chacara 86',
                'bairro'            =>'Vicente Pires',
                'cep'               =>'72005-780',
                'cidade'            =>'Brasília',
                'unidade_federacao' =>'Distrito Federal',
                'email'             =>'fernando@yahoo.ca',
                'entrada_escola'    =>'10/08/2018',
            ],
        ];

        DB::table('alunos')->insert($data);
        factory(App\Models\AlunoDisciplina::class, 3)->create();

        $notas = [
            [
                'id_aluno_disciplina' => 1,
                'nota' => 8.0
            ],
            [
                'id_aluno_disciplina' => 2,
                'nota' => 7.0
            ],
            [
                'id_aluno_disciplina' => 3,
                'nota' => 9.0
            ],

        ];

        DB::table('alunos_notas_disciplinas')->insert($notas);
    }
}
