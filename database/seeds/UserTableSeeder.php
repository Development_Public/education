<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

    public function run()
    {

        $data = [
            [
                'name'      =>'Administrator',
                'email'     =>'admin@admin.com',
                'password'  =>bcrypt('123456'),
            ],
            [
                'name'      =>'Client',
                'email'     =>'client@client.com',
                'password'  =>bcrypt('123456'),
            ]
        ];

        DB::table('users')->insert($data);


    }

}
