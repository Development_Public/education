@extends('layouts.app')

@section('title')
    Vizualizar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visualizar Disciplina
                </div>

                <div class="panel-body">
                    <p><strong>Disciplina</strong> {{ $disciplina->disciplina }}</p>
                    <p><strong>Sigla</strong> {{ $disciplina->sigla }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
