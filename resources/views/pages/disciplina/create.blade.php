@extends('layouts.app')

@section('title')
    Cadastrar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cadastrar Disciplina
                </div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'disciplina.store']) !!}

                        @include('pages.disciplina.partials.form')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
