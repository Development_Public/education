<div class="form-group">
    {{ Form::label('disciplina', 'Disciplina') }}
    {{ Form::text('disciplina', null, ['class' => 'form-control', 'id' => 'disciplina']) }}
</div>
<div class="form-group">
    {{ Form::label('sigla', 'Sigla') }}
    {{ Form::text('sigla', null, ['class' => 'form-control', 'id' => 'sigla']) }}
</div>
<div class="form-group">
    {{ Form::submit('Salvar', ['class' => 'btn btn-sm btn-primary']) }}
</div>

