@extends('layouts.app')

@section('title')
Disciplina
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Disciplinas
                    <a href="{{ route('disciplina.create') }}" class="pull-right btn btn-sm btn-primary">
                        Cadastrar
                    </a>
                </div>

                <div class="panel-body">

                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Disciplinas</th>
                                <th>Sigla</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($disciplinas as $disciplina)
                            <tr>
                                <td>{{ $disciplina->id }}</td>
                                <td>{{ $disciplina->disciplina }}</td>
                                <td>{{ $disciplina->sigla }}</td>
                                <td width="10px">
                                    <a href="{{ route('disciplina.show', $disciplina->id) }}" class="btn btn-sm btn-default">Visualizar</a>
                                </td>
                                <td width="10px">
                                    <a href="{{ route('disciplina.edit', $disciplina->id) }}" class="btn btn-sm btn-default">Editar</a>
                                </td>
                                <td width="10px">
                                    {!! Form::open(['route' => ['disciplina.destroy', $disciplina->id], 'method' => 'DELETE']) !!}
                                        <button class="btn btn-sm btn-danger">
                                            Excluir
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
