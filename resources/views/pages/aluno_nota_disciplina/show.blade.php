@extends('layouts.app')

@section('title')
    Vizualizar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visualizar Nota da Disciplina
                </div>

                <div class="panel-body">
                    <p><strong>Nota </strong> {{ $nota->nota }}</p>
                    <p><strong>Aluno Disciplina</strong> {{ $nota->id_aluno_disciplina }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
