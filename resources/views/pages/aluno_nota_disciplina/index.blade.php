@extends('layouts.app')

@section('title')
Nota da Disciplina
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Nota da Disciplina
                    <a href="{{ route('aluno_nota_disciplina.create') }}" class="pull-right btn btn-sm btn-primary">
                        Cadastrar
                    </a>
                </div>

                <div class="panel-body">

                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nota</th>
                                <th>Aluno Disciplina</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{--@foreach($nota as $notas)--}}
                            {{--<tr>--}}
                                {{--<td>{{ $nota->id }}</td>--}}
                                {{--<td>{{ $nota->nota }}</td>--}}
                                {{--<td>{{ $nota->id_aluno_disciplina }}</td>--}}
                                {{--<td width="10px">--}}
                                    {{--<a href="{{ route('aluno_nota_disciplina.show', $nota->id) }}" class="btn btn-sm btn-default">Visualizar</a>--}}
                                {{--</td>--}}
                                {{--<td width="10px">--}}
                                    {{--<a href="{{ route('aluno_nota_disciplina.edit', $nota->id) }}" class="btn btn-sm btn-default">Editar</a>--}}
                                {{--</td>--}}
                                {{--<td width="10px">--}}
                                    {{--{!! Form::open(['route' => ['aluno_nota_disciplina.destroy', $nota->id], 'method' => 'DELETE']) !!}--}}
                                        {{--<button class="btn btn-sm btn-danger">--}}
                                            {{--Excluir--}}
                                        {{--</button>--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
