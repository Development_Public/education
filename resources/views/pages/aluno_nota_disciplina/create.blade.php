@extends('layouts.app')

@section('title')
    Cadastrar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cadastrar Nota da Disciplina
                </div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'aluno_nota_disciplina.store']) !!}

                        @include('pages.aluno_nota_disciplina.partials.form')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
