@extends('layouts.app')

@section('title')
    Editar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editar Nota da Disciplina
                </div>

                <div class="panel-body">
                    {!! Form::model($nota, ['route' => ['aluno_nota_disciplina.update', $nota->id], 'method' => 'PUT']) !!}

                        @include('pages.aluno_nota_disciplina.partials.form')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
