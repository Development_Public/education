<div class="form-group">
    {{ Form::label('nota', 'Nota') }}
    {{ Form::text('nota', null, ['class' => 'form-control', 'id' => 'nota']) }}
</div>
<div class="form-group">
    {{ Form::label('id_aluno_disciplina', 'Aluno Disciplina') }}
    {{ Form::text('id_aluno_disciplina', null, ['class' => 'form-control', 'id' => 'id_aluno_disciplina']) }}
</div>
<div class="form-group">
    {{ Form::submit('Salvar', ['class' => 'btn btn-sm btn-primary']) }}
</div>

