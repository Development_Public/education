@extends('layouts.app')

@section('title')
    Editar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editar Alunos
                </div>

                <div class="panel-body">
                    {!! Form::model($aluno, ['route' => ['aluno.update', $aluno->id], 'method' => 'PUT']) !!}

                        @include('pages.aluno.partials.form')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
