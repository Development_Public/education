@extends('layouts.app')

@section('title')
    Cadastrar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cadastrar Alunos
                </div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'aluno.store']) !!}

                        @include('pages.aluno.partials.form')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
