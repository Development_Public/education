@extends('layouts.app')

@section('title')
    Vizualizar
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visualizar Alunos
                </div>

                <div class="panel-body">
                    <p><strong>Matricula:</strong>              {{ $aluno->matricula }}</p>
                    <p><strong>Nome:</strong>                   {{ $aluno->name }}</p>
                    <p><strong>Endereço:</strong>               {{ $aluno->endereco }}</p>
                    <p><strong>Bairro:</strong>                 {{ $aluno->bairro }}</p>
                    <p><strong>Cep:</strong>                    {{ $aluno->cep }}</p>
                    <p><strong>Cidade:</strong>                 {{ $aluno->cidade }}</p>
                    <p><strong>Unidade da Federação:</strong>   {{ $aluno->unidade_federacao }}</p>
                    <p><strong>email:</strong>                  {{ $aluno->email }}</p>
                    <p><strong>Data de Entrada:</strong>        {{ $aluno->entrada_escola }}</p>

                    @foreach($aluno->disciplinas  as $index => $disciplina)
                        <p>
                            <strong>Disciplinas{{$index+1}}:</strong>
                            {{ $disciplina->disciplina}} - {{ $disciplina->sigla}}
                        </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


