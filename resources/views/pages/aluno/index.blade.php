@extends('layouts.app')

@section('title')
Aluno
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Alunos
                    <a href="{{ route('aluno.create') }}" class="pull-right btn btn-sm btn-primary">
                        Cadastrar
                    </a>
                </div>

                <div class="panel-body">

                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Matricula</th>
                                <th>Nome</th>
                                <th>email</th>
                                <th>Entrada</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($alunos as $aluno)
                            <tr>
                                <td>{{ $aluno->id }}</td>
                                <td>{{ $aluno->matricula }}</td>
                                <td>{{ $aluno->name }}</td>
                                <td>{{ $aluno->email }}</td>
                                <td>{{ $aluno->entrada_escola }}</td>
                                <td width="10px">
                                    <a href="{{ route('aluno.show', $aluno->id) }}" class="btn btn-sm btn-default">Visualizar</a>
                                </td>
                                <td width="10px">
                                    <a href="{{ route('aluno.edit', $aluno->id) }}" class="btn btn-sm btn-default">Editar</a>
                                </td>
                                <td width="10px">
                                    {!! Form::open(['route' => ['aluno.destroy', $aluno->id], 'method' => 'DELETE']) !!}
                                        <button class="btn btn-sm btn-danger">
                                            Excluir
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
