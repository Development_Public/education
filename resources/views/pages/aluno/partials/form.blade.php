<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('matricula', 'Matricula') }}
        {{ Form::text('matricula', null, ['class' => 'form-control', 'id' => 'matricula']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('name', 'Nome') }}
        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('endereco', 'Endereço') }}
        {{ Form::text('endereco', null, ['class' => 'form-control', 'id' => 'endereco']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('bairro', 'Bairro') }}
        {{ Form::text('bairro', null, ['class' => 'form-control', 'id' => 'bairro']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('cep', 'Cep') }}
        {!! Form::text('cep', null, ['class' => 'form-control','id' => 'cep']) !!}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('cidade', 'Cidade') }}
        {{ Form::text('cidade', null, ['class' => 'form-control', 'id' => 'cidade']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('unidade_federacao', 'Unidade da Federação') }}
        {{ Form::text('unidade_federacao', null, ['class' => 'form-control', 'id' => 'unidade_federacao']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('email', 'E-mail') }}
        {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        {{ Form::label('entrada_escola', 'Data de Entrada') }}
        {!! Form::date('entrada_escola', null, ['class' => 'form-control','id' => 'entrada_escola']) !!}
    </div>
</div>

{{--<div class="col-md-6">--}}
    {{--<div class="form-group">--}}

            {{--{{ Form::label('disciplinas', 'Disciplinas') }}--}}
            {{--<select class="form-control" name="disciplinas">--}}
            {{--@foreach(App\Models\Disciplina::all() as $disciplina)--}}
                {{--<option value='{{ $disciplina->id }}'>{{ $disciplina->disciplina}} - {{ $disciplina->sigla}}</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}
    {{--</div>--}}


        {{--<div class="form-group">--}}
            {{--{!! Form::Label('disciplinas', 'Disciplinas:') !!}--}}
            {{--<select class="form-control" name="disciplinas">--}}
                {{--@foreach($aluno->disciplinas  as $index => $disciplina)--}}
                    {{--<option value="{{$disciplina->id}}">{{ $disciplina->disciplina}} - {{ $disciplina->sigla}}</option>--}}
                {{--@endforeach--}}
            {{--</select>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}



<div class="form-group">
    {{ Form::submit('Salvar', ['class' => 'btn btn-sm btn-primary']) }}
</div>

