<?php

namespace App\Http\Controllers;

use App\Models\AlunoNotaDisciplina;
use Illuminate\Http\Request;
use App\Http\Requests\AlunoNotaDisciplinaStoreRequest;
use App\Http\Requests\AlunoNotaDisciplinaUpdateRequest;

class AlunosNotaDisciplinasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nota = AlunoNotaDisciplina::orderBy('id', 'DESC')->with(['alunoDisciplina'])->paginate();


//        return response()->json($nota);
        return view('pages.aluno_nota_disciplina.index', compact('nota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.aluno_nota_disciplina.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlunoNotaDisciplinaStoreRequest $request)
    {
        $nota = AlunoNotaDisciplina::create($request->all());

        return redirect()->route('aluno_nota_disciplina.index', $nota->id)->with('info', 'Nota da Disciplina Criada com Sucesso !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nota = AlunoNotaDisciplina::find($id);

        return view('pages.aluno_nota_disciplina.show', compact('nota'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nota = AlunoNotaDisciplina::find($id);

        return view('pages.aluno_nota_disciplina.edit', compact('nota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlunoNotaDisciplinaUpdateRequest $request, $id)
    {
        $nota = AlunoNotaDisciplina::find($id);

        $nota->fill($request->all())->save();

        return redirect()->route('aluno_nota_disciplina.index', $nota->id)->with('info', 'Nota da Disciplina Atualizada com Sucesso !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nota = AlunoNotaDisciplina::find($id)->delete();

        return back()->with('info', 'Nota da Disciplina Excluida com Sucesso !');
    }
}
