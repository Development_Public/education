<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Prettus\Repository;

trait ApiControllerTrait
{
    public function index()
    {
        $query = $this->repository->paginate($limit   = null, $columns = ['*']);
        return Response::json($query);
    }

    public function store(Request $request)
    {
        $query = $this->repository->create($request->all());
        return Response::json($query);
    }

    public function show($id)
    {
        $query = $this->repository->with($this->relationships())->find($id);
        return Response::json($query);
    }

    public function update(Request $request, $id)
    {
        $query = $this->repository->find($id);
        $query->update($request->all());
        return Response::json($query);
    }

    public function destroy($id)
    {
        $query = $this->repository->find($id);
        $query->delete();
        return Response::json($query);
    }

    protected function relationships()
    {
        if (isset($this->relationships)) {
            return $this->relationships;
        }
        return [];
    }

}
