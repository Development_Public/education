<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\AlunosRepository;

class ApiAlunoController extends Controller
{
    use \App\Http\Controllers\ApiControllerTrait;

    private $repository;

    //protected $relationships = ['disciplina'];

    public function __construct(AlunosRepository $alunosRepository)
    {
        $this->repository = $alunosRepository;
    }

}
