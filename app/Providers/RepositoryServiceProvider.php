<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\UserRepository',
            'App\Repositories\UserRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\AlunosRepository',
            'App\Repositories\AlunosRepositoryEloquent'
        );


        $this->app->bind(
            'App\Repositories\DisciplpinaRepository',
            'App\Repositories\DisciplinaRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\AlunoNotaDisciplinaRepository',
            'App\Repositories\AlunoNotaDisciplinaRepositoryEloquent'
        );



    }
}
