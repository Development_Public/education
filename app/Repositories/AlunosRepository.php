<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AlunosRepository
 * @package namespace App\Repositories;
 */
interface AlunosRepository extends RepositoryInterface
{
    //
}
