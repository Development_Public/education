<?php

namespace App\Repositories;

use App\Models\Aluno;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AlunosRepository;

/**
 * Class DisciplinaRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AlunosRepositoryEloquent extends BaseRepository implements AlunosRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Aluno::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        // $this->pushCriteria(app(RequestCriteria::class));
    }

}
