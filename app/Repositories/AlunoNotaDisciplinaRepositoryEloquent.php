<?php

namespace App\Repositories;

use App\Models\AlunoNotaDisciplina;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AlunoNotaDisciplinaRepository;

/**
 * Class AlunoNotaDisciplinaRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AlunoNotaDisciplinaRepositoryEloquent extends BaseRepository implements AlunoNotaDisciplinaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AlunoNotaDisciplina::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        // $this->pushCriteria(app(RequestCriteria::class));
    }

}
