<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AlunoNotaDisciplinaRepository
 * @package namespace App\Repositories;
 */
interface AlunoNotaDisciplinaRepository extends RepositoryInterface
{
    //
}
