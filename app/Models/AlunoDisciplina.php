<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlunoDisciplina extends Model
{
    protected $table = 'alunos_disciplinas';
    public $timestamps = false;

    /**
     * AlunoDisciplina has many Nota.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notas()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = alunoDisciplina_id, localKey = id)
        return $this->hasMany('App\Models\AlunoNotaDisciplina', 'id_aluno_disciplina', 'id');
    }

    public function aluno()
    {
        return $this->belongsTo('App\Models\Aluno', 'id_aluno', 'id');
    }
}
