<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Aluno extends Model implements Transformable
{

    use TransformableTrait;

    protected $table = 'alunos';

    protected $fillable = [
        'matricula',
        'name',
        'endereco',
        'bairro',
        'cep',
        'cidade',
        'unidade_federacao',
        'email',
        'entrada_escola',
    ];

    /**
     * Aluno belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function disciplinas()
    {
        // belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
        return $this->belongsToMany('App\Models\Disciplina', 'alunos_disciplinas', 'id_aluno', 'id_disciplina');
    }
}
