<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Disciplina extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'disciplinas';

    protected $fillable = [
        'disciplina',
        'sigla'
    ];

     /**
     * Aluno belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function alunos()
    {
        return $this->belongsToMany('App\Models\Aluno', 'alunos_disciplinas', 'id_disciplina', 'id_aluno');
    }
}
