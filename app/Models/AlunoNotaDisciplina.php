<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AlunoNotaDisciplina extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'alunos_notas_disciplinas';

    protected $fillable = [
        'nota',
        'id_aluno_disciplina'
    ];

    public $timestamps = false;

    /**
     * AlunoNotaDisciplina belongs to Aluno.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function alunoDisciplina()
    {
        // belongsTo(RelatedModel, foreignKey = aluno_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\Models\AlunoDisciplina', 'id_aluno_disciplina', 'id');
    }
}
